package mnjava.derin.domain.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

public class RekamMedikCreateOld {
    @NotNull(message = "IdPasien Tidak Boleh Kosong")
    protected Long idPasien;

    @NotNull(message = "IdPenyakit Tidak Boleh Kosong")
    protected Long idPenyakit;

    @NotNull(message = "Id Gejala Tidak Boleh Kosong")
    @Size(min=0, message = "Minimal satu gejala diperlukan")
    protected List<Long> idListGejala;

    @NotNull(message = "Tanggal Tidak Boleh Kosong")
    protected LocalDate tanggal;

    @NotNull(message = "Tanggal Tidak Boleh Kosong")
    protected boolean basisKasus;

    public RekamMedikCreateOld() {
    }

    public Long getIdPasien() {
        return idPasien;
    }

    public void setIdPasien(Long idPasien) {
        this.idPasien = idPasien;
    }

    public Long getIdPenyakit() {
        return idPenyakit;
    }

    public void setIdPenyakit(Long idPenyakit) {
        this.idPenyakit = idPenyakit;
    }

    public List<Long> getIdListGejala() {
        return idListGejala;
    }

    public void setIdListGejala(List<Long> idListGejala) {
        this.idListGejala = idListGejala;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public boolean isBasisKasus() {
        return basisKasus;
    }

    public void setBasisKasus(boolean basisKasus) {
        this.basisKasus = basisKasus;
    }

    @Override
    public String toString() {
        return "RekamMedikCreateOld{" +
        "idPasien=" + idPasien +
        ", idPenyakit=" + idPenyakit +
        ", idListGejala=" + idListGejala +
        ", tanggal=" + tanggal +
        ", basisKasus=" + basisKasus +
        '}';
    }
}

package mnjava.derin.domain.dto;

import mnjava.derin.domain.Kasus;
import mnjava.derin.domain.TipeKasus;

public class KasusDTO {

    protected Long rekamMedikId;
    protected Long id;
    protected TipeKasus type;
    protected Long penyakitId;
    protected String penyakitNama;

    public Long getRekamMedikId() {
        return rekamMedikId;
    }

    public void setRekamMedikId(Long rekamMedikId) {
        this.rekamMedikId = rekamMedikId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipeKasus getType() {
        return type;
    }

    public void setType(TipeKasus type) {
        this.type = type;
    }

    public Long getPenyakitId() {
        return penyakitId;
    }

    public void setPenyakitId(Long penyakitId) {
        this.penyakitId = penyakitId;
    }

    public String getPenyakitNama() {
        return penyakitNama;
    }

    public void setPenyakitNama(String penyakitNama) {
        this.penyakitNama = penyakitNama;
    }
}

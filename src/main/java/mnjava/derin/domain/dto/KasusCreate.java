package mnjava.derin.domain.dto;

import mnjava.derin.domain.TipeKasus;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class KasusCreate {

    @NotNull
    protected Long idRekamMedik;

    @NotNull
    protected TipeKasus type;

    @Nullable
    protected Double similarity;

    public Long getIdRekamMedik() {
        return idRekamMedik;
    }

    public void setIdRekamMedik(Long idRekamMedik) {
        this.idRekamMedik = idRekamMedik;
    }

    public TipeKasus getType() {
        return type;
    }

    public void setType(TipeKasus type) {
        this.type = type;
    }

    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Double similarity) {
        this.similarity = similarity;
    }
}

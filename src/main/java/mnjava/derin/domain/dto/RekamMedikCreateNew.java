package mnjava.derin.domain.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

public class RekamMedikCreateNew {
    @NotBlank(message = "Nama Tidak Boleh Kosong")
    protected String nama;

    @NotBlank(message = "Alamat Tidak boleh kosong")
    protected String alamat;

    @NotNull(message = "Jenis Kelamin Tidak Boleh Kosong")
    protected Boolean woman;

    @NotNull(message = "Tanggal Tidak Boleh Kosong")
    protected LocalDate tanggalLahir;

    @NotBlank(message = "Alamat Tidak boleh kosong")
    protected String tempatLahir;

    @NotNull(message = "IdPenyakit Tidak Boleh Kosong")
    protected Long idPenyakit;

    @NotNull(message = "Id Gejala Tidak Boleh Kosong")
    @Size(min=0, message = "Minimal satu gejala diperlukan")
    protected List<Long> idListGejala;

    @NotNull(message = "Tanggal Tidak Boleh Kosong")
    protected LocalDate tanggal;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Boolean getWoman() {
        return woman;
    }

    public void setWoman(Boolean woman) {
        this.woman = woman;
    }

    public LocalDate getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(LocalDate tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public Long getIdPenyakit() {
        return idPenyakit;
    }

    public void setIdPenyakit(Long idPenyakit) {
        this.idPenyakit = idPenyakit;
    }

    public List<Long> getIdListGejala() {
        return idListGejala;
    }

    public void setIdListGejala(List<Long> idListGejala) {
        this.idListGejala = idListGejala;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }
}

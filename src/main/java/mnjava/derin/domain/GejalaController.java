package mnjava.derin.domain;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.spring.tx.annotation.Transactional;
import io.micronaut.validation.Validated;
import java.util.Optional;

@Controller("/api/v1/gejala")
@Validated
public class GejalaController {

    private GejalaRepo gejalaRepo;

    public GejalaController(GejalaRepo gejalaRepo) {
        this.gejalaRepo = gejalaRepo;
    }

    @Get("/")
    public Page<Gejala> findAll(
            @QueryValue(value= "from", defaultValue="0") Integer from,
            @QueryValue(value = "size", defaultValue = "20") Integer size
    )  {
        return this.gejalaRepo.findAll(Pageable.from(from, size));
    }

    @Get("/{id}")
    public Optional<Gejala> findOne(Long id) {
        return this.gejalaRepo.findById(id);
    }

    @Post(value = "/", produces = MediaType.TEXT_PLAIN)
    public Long create(@Body Gejala payload) {
        Gejala gejala = this.gejalaRepo.save(payload);
        return gejala.getId();
    }

    @Put("/{id}")
    @Transactional
    public void update(Long id, @Body Gejala payload) {
        Optional<Gejala> option = this.gejalaRepo.findById(id);
        option.ifPresent(gejala -> {
            gejala.setNama(payload.getNama());
            gejala.setDeskripsi(payload.getDeskripsi());
            gejala.setWoman(payload.getWoman());
            this.gejalaRepo.save(gejala);
        });
    }

    @Delete("/{id}")
    public void delete(Long id) {
        this.gejalaRepo.deleteById(id);
    }

}

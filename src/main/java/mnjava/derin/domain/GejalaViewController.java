package mnjava.derin.domain;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Controller("/api/v1/gejala-view")
public class GejalaViewController {

    private GejalaRepo gejalaRepo;

    public GejalaViewController(GejalaRepo gejalaRepo) {
        this.gejalaRepo = gejalaRepo;
    }

    @Transactional
    @Get("/sex-keyword")
    public List<Gejala> findBySexAndKeyword(
        @QueryValue(value= "keyword", defaultValue="") String keyword,
        @NotBlank @QueryValue(value = "sex") String sex
    ) {
        return this.gejalaRepo.findBySexAndKeyword(sex, keyword);
    }
}

package mnjava.derin.domain;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Controller("/api/v1/penyakit-view")
public class PenyakitViewController {

    protected PenyakitRepo penyakitRepo;

    public PenyakitViewController(PenyakitRepo penyakitRepo) {
        this.penyakitRepo = penyakitRepo;
    }

    @Transactional
    @Get("/sex-keyword")
    public List<Penyakit> findBySexAndKeyword(
        @QueryValue(value= "keyword", defaultValue="") String keyword,
        @NotBlank @QueryValue(value = "sex") String sex
    ) {
        return this.penyakitRepo.findBySexAndKeyword(keyword, sex);
    }
}

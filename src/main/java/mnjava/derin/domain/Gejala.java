package mnjava.derin.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Table(name="gejala")
@Entity
public class Gejala {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nama", nullable = false)
    @NotNull
    private String nama;

    @Column(name="deskripsi", nullable = false)
    private String deskripsi;

    @Column(name="woman", nullable = false)
    @NotNull
    private Boolean woman;

    @Column(name="man", nullable = false)
    @NotNull
    private Boolean man;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = {
        CascadeType.PERSIST,
        CascadeType.MERGE
    })
    @JoinTable(
        name="rm_gj",
        joinColumns = @JoinColumn(name="Gj_id"),
        inverseJoinColumns = @JoinColumn(name="RM_id")
    )
    protected Set<RekamMedik> rekamMedikSet = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Boolean getWoman() {
        return woman;
    }

    public void setWoman(Boolean woman) {
        this.woman = woman;
    }

    public Boolean getMan() {
        return man;
    }

    public void setMan(Boolean man) {
        this.man = man;
    }

    public Set<RekamMedik> getRekamMedikSet() {
        return rekamMedikSet;
    }

    public boolean addRekamMedik(RekamMedik rekamMedik) {
        return rekamMedikSet.add(rekamMedik);
    }

    public boolean removeRekamMedik(Object o) {
        return rekamMedikSet.remove(o);
    }

    public void clearRekamMedik() { rekamMedikSet.clear(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gejala gejala = (Gejala) o;

        return getId().equals(gejala.getId());

    }

    @Override
    public int hashCode() {
        return 32;
    }
}

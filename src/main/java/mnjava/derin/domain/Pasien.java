package mnjava.derin.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Table(name="pasien")
@Entity
public class Pasien {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nama", nullable = false)
    private String nama;

    @Column(name="alamat", nullable = false)
    private String alamat;

    @Column(name="woman", nullable = false)
    private Boolean woman;

    @Column(name="tanggalLahir", nullable = false)
    private LocalDate tanggalLahir;

    @Column(name="tempatLahir", nullable = false)
    private String tempatLahir;

    @JsonIgnore
    @OneToMany(mappedBy = "pasien", cascade = CascadeType.REMOVE)
    protected Set<RekamMedik> rekamMedikSet = new HashSet<>();

    public Set<RekamMedik> getRekamMedikSet() {
        return rekamMedikSet;
    }

    public boolean addRekamMedik(RekamMedik rekamMedik) {
        return rekamMedikSet.add(rekamMedik);
    }

    public boolean removeRekamMedik(RekamMedik o) {
        return rekamMedikSet.remove(o);
    }

    public void clearRekamMedik() {
        rekamMedikSet.clear();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Boolean getWoman() {
        return woman;
    }

    public void setWoman(Boolean woman) {
        this.woman = woman;
    }

    public LocalDate getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(LocalDate tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pasien pasien = (Pasien) o;

        return getId() != null ? getId().equals(pasien.getId()) : pasien.getId() == null;
    }

    @Override
    public int hashCode() {
        return 0;
    }
}

package mnjava.derin.domain;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.spring.tx.annotation.Transactional;
import io.micronaut.validation.Validated;
import mnjava.derin.domain.dto.KasusCreate;
import mnjava.derin.domain.dto.KasusDTO;

import java.util.Optional;

@Controller("/api/v1/basis-kasus")
@Validated
public class BasisKasusController {

    protected KasusRepo kasusRepo;

    public BasisKasusController(KasusRepo kasusRepo) {
        this.kasusRepo = kasusRepo;
    }

    @Post(value = "/", produces = MediaType.TEXT_PLAIN)
    public Long create(KasusCreate payload) {
        return this.kasusRepo.create(payload);
    }

    @Get("/{id}")
    public Optional<KasusDTO> findOne(Long id) {
        return this.kasusRepo.findOne(id);
    }

    @Get("/")
    @Transactional
    public Page<KasusDTO> findAll(
        @QueryValue(value= "from", defaultValue="0") Integer from,
        @QueryValue(value = "size", defaultValue = "20") Integer size
    )  {
        return this.kasusRepo.find(Pageable.from(from, size));
    }

    @Delete("/{id}")
    public void delete(Long id) {
        this.kasusRepo.delete(id);
    }

}

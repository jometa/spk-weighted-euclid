package mnjava.derin.domain;

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository("domain")
public abstract class PenyakitRepo implements PageableRepository<Penyakit, Long> {
    @PersistenceContext
    private final EntityManager entityManager;

    public PenyakitRepo(@CurrentSession EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Penyakit> findBySexAndKeyword(@NotNull String keyword, @NotBlank String sex) {
        String queryS = "SELECT p FROM Penyakit p WHERE lower(p.nama) LIKE lower(concat('%', :keyword, '%')) AND ";
        switch (sex) {
            case "woman":
                queryS = queryS + "p.woman = TRUE";
                break;
            case "man":
                queryS = queryS + "p.man = TRUE";
                break;
            default:
                throw new RuntimeException("Unknwon sex: " + sex);
        }
        Query query = this.entityManager.createQuery(queryS);
        query.setParameter("keyword", keyword);
        List<Penyakit> results = query.getResultList();
        return results;
    }
}

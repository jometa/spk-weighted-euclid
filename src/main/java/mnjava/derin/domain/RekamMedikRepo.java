package mnjava.derin.domain;

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.Join;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;
import io.micronaut.spring.tx.annotation.Transactional;
import mnjava.derin.domain.dto.KasusCreate;
import mnjava.derin.domain.dto.RekamMedikCreateNew;
import mnjava.derin.domain.dto.RekamMedikCreateOld;
import org.springframework.transaction.annotation.Propagation;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository("domain")
public abstract class RekamMedikRepo implements PageableRepository<RekamMedik, Long> {

    protected EntityManager em;
    protected KasusRepo kasusRepo;

    public RekamMedikRepo(
        @CurrentSession EntityManager em,
        KasusRepo kasusRepo
    ) {
        this.em = em;
        this.kasusRepo = kasusRepo;
    }

    @Join(value = "penyakit", type = Join.Type.FETCH)
    @Join(value = "pasien", type = Join.Type.FETCH)
    @Join(value = "gejalas", type = Join.Type.FETCH)
    @Join(value = "kasus", type = Join.Type.LEFT_FETCH)
    abstract Page<RekamMedik> listPage(Pageable pageable);

    @Join(value = "penyakit", type = Join.Type.FETCH)
    @Join(value = "pasien", type = Join.Type.FETCH)
    @Join(value = "gejalas", type = Join.Type.FETCH)
    @Join(value = "kasus", type = Join.Type.LEFT_FETCH)
    abstract Optional<RekamMedik> findFirstById(Long id);

    public Long createNewPasien(RekamMedikCreateNew payload) {
        Pasien pasien = new Pasien();
        pasien.setNama(payload.getNama());
        pasien.setAlamat(payload.getAlamat());
        pasien.setTempatLahir(payload.getTempatLahir());
        pasien.setTanggalLahir(payload.getTanggalLahir());
        pasien.setWoman(payload.getWoman());

        em.persist(pasien);
        Penyakit penyakit = em.getReference(Penyakit.class, payload.getIdPenyakit());

        RekamMedik rekamMedik = new RekamMedik();
        rekamMedik.setPasien(pasien);
        rekamMedik.setPenyakit(penyakit);
        rekamMedik.setTanggal(payload.getTanggal());

        for (Long gid : payload.getIdListGejala()) {
            Gejala g = em.getReference(Gejala.class, gid);
            g.addRekamMedik(rekamMedik);
            rekamMedik.addGejala(g);
        }

        em.persist(rekamMedik);

        return rekamMedik.getId();
    }

    public Long createOldPasien(RekamMedikCreateOld payload) {
        Pasien pasien = em.getReference(Pasien.class, payload.getIdPasien());
        Penyakit penyakit = em.getReference(Penyakit.class, payload.getIdPenyakit());
        RekamMedik rekamMedik = new RekamMedik();
        rekamMedik.setPasien(pasien);
        rekamMedik.setPenyakit(penyakit);
        rekamMedik.setTanggal(payload.getTanggal());

        for (Long gid : payload.getIdListGejala()) {
           Gejala g = em.getReference(Gejala.class, gid);
           g.addRekamMedik(rekamMedik);
           rekamMedik.addGejala(g);
        }
        em.persist(rekamMedik);

        if (payload.isBasisKasus()) {
            KasusCreate kasusPayload = new KasusCreate();
            kasusPayload.setIdRekamMedik(rekamMedik.getId());
            kasusPayload.setSimilarity(1D);
            kasusPayload.setType(TipeKasus.BASIS_KASUS);
            this.kasusRepo.create(kasusPayload);
        }

        return rekamMedik.getId();
    }

    public void update(Long id, RekamMedikCreateOld payload) {
        Optional<RekamMedik> optional = this.findFirstById(id);
        optional.ifPresent(rekamMedik -> {
            Pasien pasien = this.em.getReference(Pasien.class, payload.getIdPasien());
            Penyakit penyakit = this.em.getReference(Penyakit.class, payload.getIdPenyakit());

            rekamMedik.setPenyakit(penyakit);
            rekamMedik.setPasien(pasien);
            rekamMedik.setTanggal(payload.getTanggal());

            for (Long gId : payload.getIdListGejala()) {
                Gejala g = em.getReference(Gejala.class, gId);
                g.addRekamMedik(rekamMedik);
                rekamMedik.addGejala(g);
            }

            this.em.merge(rekamMedik);
        });
    }

    public abstract void updateTanggal(@Id Long id, LocalDate tanggal);

    public void updatePasien(Long id, Long idPasien) {
        Pasien pasien = this.em.getReference(Pasien.class, idPasien);
        RekamMedik rm = this.em.getReference(RekamMedik.class, id);
        rm.setPasien(pasien);
        this.em.merge(rm);
    }

    public void addGejala(Long id, Long idGejala) {
        RekamMedik rm = this.em.getReference(RekamMedik.class, id);
        Gejala gejala = this.em.getReference(Gejala.class, idGejala);
        rm.addGejala(gejala);
        gejala.addRekamMedik(rm);
        this.em.merge(rm);
        this.em.merge(gejala);
    }

    public void deleteGejala(Long id, Long idGejala) {
        // Using getReference doesn't bring the relations into state.
        // Seems like old Native SQL is the way.
        Query query = em.createNativeQuery("DELETE FROM RM_Gj WHERE RM_id = :rmid AND GJ_id = :gjid");
        query.setParameter("rmid", id);
        query.setParameter("gjid", idGejala);
        query.executeUpdate();
    }

    public void updatePenyakit(Long id, Long idPenyakit) {
        Penyakit penyakit = this.em.getReference(Penyakit.class, idPenyakit);
        RekamMedik rm = this.em.getReference(RekamMedik.class, id);
        rm.setPenyakit(penyakit);
        this.em.merge(rm);
    }
}

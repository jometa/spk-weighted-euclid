package mnjava.derin.domain;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.*;
import io.micronaut.spring.tx.annotation.Transactional;
import io.micronaut.validation.Validated;

import java.util.Optional;

@Controller("/api/v1/pasien")
@Validated
public class PasienController {

    protected PasienRepo pasienRepo;

    public PasienController(PasienRepo pasienRepo) {
        this.pasienRepo = pasienRepo;
    }

    @Get("/")
    public Page<Pasien> find(
        @QueryValue(value = "from", defaultValue = "0") Integer from,
        @QueryValue(value = "size", defaultValue = "20") Integer size
    ) {
        return this.pasienRepo.findAll(Pageable.from(from, size));
    }

    @Get("/{id}")
    public Optional<Pasien> findOne(Long id) {
        return this.pasienRepo.findById(id);
    }

    @Post("/")
    public Long create(@Body Pasien payload) {
        Pasien pasien = this.pasienRepo.save(payload);
        return pasien.getId();
    }

    @Put("/{id}")
    @Transactional
    public void update(Long id, @Body Pasien payload) {
        Optional<Pasien> optional = this.pasienRepo.findById(id);
        optional.ifPresent(pasien -> {
            pasien.setNama(payload.getNama());
            pasien.setWoman(payload.getWoman());
            pasien.setTanggalLahir(payload.getTanggalLahir());
            pasien.setTempatLahir(payload.getTempatLahir());
            pasien.setAlamat(payload.getAlamat());

            this.pasienRepo.save(pasien);
        });
    }

    @Delete("/{id}")
    public void delete(Long id) {
        this.pasienRepo.deleteById(id);
    }
}

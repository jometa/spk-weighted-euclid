package mnjava.derin.domain.aggregate;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import mnjava.derin.Util;
import mnjava.derin.domain.Penyakit;

import java.util.List;

@Controller("/api/v1/aggregate/penyakit")
public class PenyakitAggregateController {

    protected PenyakitAggregateService service;

    public PenyakitAggregateController(PenyakitAggregateService service) {
        this.service = service;
    }

    @Get("/basiskasus/count")
    public List<Util.Pair<Penyakit, Long>> count() {
        return this.service.countInBasisKasus();
    }
}

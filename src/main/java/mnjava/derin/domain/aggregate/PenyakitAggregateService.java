package mnjava.derin.domain.aggregate;

import io.micronaut.spring.tx.annotation.Transactional;
import mnjava.derin.Util;
import mnjava.derin.domain.Penyakit;
import org.hibernate.transform.ResultTransformer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

public class PenyakitAggregateService {
    private static final String MYSQL_COUNT_IN_BASIS_KASUS = "" +
    "SELECT p.id, p.nama, p.deskripsi, p.solusi, COUNT(k.id) AS total" +
    "       FROM penyakit p " +
    "       LEFT JOIN rekammedik rm ON rm.Penyakit_id = p.id" +
    "       LEFT JOIN kasus k on k.id = rm.kasus_id" +
    "       GROUP BY p.id";

    @PersistenceContext
    protected EntityManager em;

    public PenyakitAggregateService(EntityManager em) {
        this.em = em;
    }

    @Transactional
    public List<Util.Pair<Penyakit, Long>> countInBasisKasus() {
        Query query = this.em.createNativeQuery(MYSQL_COUNT_IN_BASIS_KASUS);
        return query.unwrap(org.hibernate.query.Query.class)
             .setResultTransformer(
                 new ResultTransformer() {
                     @Override
                     public Object transformTuple(Object[] tuple, String[] aliases) {
                         Penyakit p = new Penyakit();

                         p.setId( ((BigInteger) tuple[0]).longValue() );
                         p.setNama((String) tuple[1]);
                         p.setDeskripsi((String) tuple[2]);
                         p.setSolusi((String) tuple[3]);
                         Long total = ( (BigInteger) tuple[4] ).longValue();
                         return Util.Pair.of(p, total);
                     }

                     @Override
                     public List transformList(List collection) {
                         return collection;
                     }
                 }
             )
             .getResultList();
    }
}

package mnjava.derin.domain;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

@Repository("domain")
public interface PasienRepo extends PageableRepository<Pasien, Long> {
}

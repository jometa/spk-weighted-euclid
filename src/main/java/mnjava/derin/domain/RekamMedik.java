package mnjava.derin.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import mnjava.derin.algo.Row;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Table(name="rekammedik")
@Entity
public class RekamMedik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Pasien_id")
    private Pasien pasien;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Penyakit_id")
    private Penyakit penyakit;

    @ManyToMany(mappedBy = "rekamMedikSet")
    private Set<Gejala> gejalas = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="Kasus_id")
    protected Kasus kasus;

    @Column(name="tanggal", nullable = false)
    protected LocalDate tanggal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pasien getPasien() {
        return pasien;
    }

    public void setPasien(Pasien pasien) {
        this.pasien = pasien;
    }

    public Penyakit getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(Penyakit penyakit) {
        this.penyakit = penyakit;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Kasus getKasus() {
        return kasus;
    }

    public void setKasus(Kasus kasus) {
        this.kasus = kasus;
    }

    public Set<Gejala> getGejalas() {
        return this.gejalas;
    }

    public boolean addGejala(Gejala gejala) {
        return gejalas.add(gejala);
    }

    public boolean removeGejala(Object o) {
        return gejalas.remove(o);
    }

    public void clearGejala() {
        gejalas.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RekamMedik that = (RekamMedik) o;

        return getId() != null ? getId().equals(that.getId()) : that.getId() == null;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    public Row toRow() {
        return Row.Builder
            .id(this.id)
            .attributes(
                this.gejalas.stream()
                    .map(g -> g.getId())
                    .toArray(Long[]::new)
            )
            .target(this.penyakit.getId());
    }
}

package mnjava.derin.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table(name="penyakit")
@Entity
public class Penyakit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nama", nullable = false)
    private String nama;

    @Column(name="deskripsi", nullable = false)
    private String deskripsi;

    @Column(name="solusi", nullable = false)
    private String solusi;

    @Column(name="woman", nullable = false)
    private Boolean woman;

    @Column(name="man", nullable = false)
    private Boolean man;

    @JsonIgnore
    @OneToMany(mappedBy = "penyakit", cascade = CascadeType.REMOVE)
    protected Set<RekamMedik> rekamMedikSet = new HashSet<>();

    public Set<RekamMedik> getRekamMedikSet() {
        return rekamMedikSet;
    }
    public void addRekamMedik(RekamMedik rm) {
        this.rekamMedikSet.add(rm);
    }
    public void deleteRekamMedik(RekamMedik rm) {
        this.rekamMedikSet.remove(rm);
    }
    public void clearRekamMedik() {
        this.rekamMedikSet.clear();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getSolusi() {
        return solusi;
    }

    public void setSolusi(String solusi) {
        this.solusi = solusi;
    }

    public Boolean getWoman() {
        return woman;
    }

    public void setWoman(Boolean woman) {
        this.woman = woman;
    }

    public Boolean getMan() {
        return man;
    }

    public void setMan(Boolean man) {
        this.man = man;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Penyakit penyakit = (Penyakit) o;

        return getId().equals(penyakit.getId());
    }

    @Override
    public int hashCode() {
        return 0;
    }
}

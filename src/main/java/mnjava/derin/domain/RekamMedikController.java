package mnjava.derin.domain;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.*;
import io.micronaut.spring.tx.annotation.Transactional;
import io.micronaut.validation.Validated;
import mnjava.derin.domain.dto.RekamMedikCreateOld;

import java.time.LocalDate;
import java.util.Optional;

@Controller("/api/v1/rekam-medik")
@Validated
public class RekamMedikController {

    protected RekamMedikRepo rekamMedikRepo;

    public RekamMedikController(RekamMedikRepo rekamMedikRepo) {
        this.rekamMedikRepo = rekamMedikRepo;
    }

    @Get("/")
    public Page<RekamMedik> find(
        @QueryValue(value = "from", defaultValue = "0") Integer from,
        @QueryValue(value = "size", defaultValue = "20") Integer size
    ) {
        Page<RekamMedik> rekamMediks = this.rekamMedikRepo.listPage(Pageable.from(from, size));
        System.out.println("rekamMediks=" + rekamMediks);
        return rekamMediks;
    }

    @Get("/{id}")
    public Optional<RekamMedik> findOne(Long id) {
        return this.rekamMedikRepo.findFirstById(id);
    }

    @Delete("/{id}")
    public void delete(Long id) {
        this.rekamMedikRepo.deleteById(id);
    }

    @Post("/")
    @Transactional
    public Long createWithOldPasien(@Body RekamMedikCreateOld payload) {
        Long pasienId = this.rekamMedikRepo.createOldPasien(payload);
        return pasienId;
    }

    @Put("/{id}")
    @Transactional
    public void update(Long id, @Body RekamMedikCreateOld payload) {
        this.rekamMedikRepo.update(id, payload);
    }

    // Methods for update specific field of RekamMedik.
    @Patch("/{id}/tanggal/{tanggal}")
    @Transactional
    public void updateTanggal(Long id, String tanggal) {
        LocalDate date = LocalDate.parse(tanggal);
        this.rekamMedikRepo.updateTanggal(id, date);
    }

    @Patch("/{id}/pasien/{idPasien}")
    @Transactional
    public void updatePasien(Long id, Long idPasien) {
        this.rekamMedikRepo.updatePasien(id, idPasien);
    }

    @Patch("/{id}/penyakit/{idPenyakit}")
    @Transactional
    public void updatePenyakit(Long id, Long idPenyakit) {
        this.rekamMedikRepo.updatePenyakit(id, idPenyakit);
    }

    @Post("/{id}/gejala/{idGejala}")
    @Transactional
    public void addGejala(Long id, Long idGejala) {
        this.rekamMedikRepo.addGejala(id, idGejala);
    }

    @Delete("/{id}/gejala/{idGejala}")
    @Transactional
    public void deleteGejala(Long id, Long idGejala) {
        System.out.println("HERE");
        this.rekamMedikRepo.deleteGejala(id, idGejala);
    }
}

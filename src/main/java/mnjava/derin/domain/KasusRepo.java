package mnjava.derin.domain;

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.spring.tx.annotation.Transactional;
import mnjava.derin.algo.Row;
import mnjava.derin.domain.dto.KasusCreate;
import mnjava.derin.domain.dto.KasusDTO;
import org.hibernate.transform.ResultTransformer;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton
public class KasusRepo {

    @PersistenceContext
    protected EntityManager em;

    protected final String JPQL_FIND_KASUS= "" +
    "   SELECT DISTINCT k FROM Kasus k" +
    "       JOIN FETCH k.rekamMedik rm" +
    "       JOIN FETCH rm.pasien pas" +
    "       JOIN FETCH rm.penyakit p" +
    "       JOIN FETCH rm.gejalas glist";

    protected final String JPQL_FIND_KASUS_WITH_RELATIONS= "" +
        "   SELECT DISTINCT k FROM Kasus k" +
        "       JOIN FETCH k.rekamMedik rm" +
        "       JOIN FETCH rm.pasien pas" +
        "       JOIN FETCH rm.penyakit p" +
        "       JOIN FETCH rm.gejalas glist" +
        "       WHERE k.id = :id";

    protected final String NATIVE_SQL_FIND_KASUS_FOR_SEX = "SELECT\n" +
    "        k.id as id,\n" +
    "        rm.id as rmid,\n" +
    "        p.id as pid,\n" +
    "        p.nama as pnama,\n" +
    "        k.type as type,\n" +
    "        GROUP_CONCAT(g.id) as gids\n" +
    "        \n" +
    "        \tFROM kasus k\n" +
    "        \t\tJOIN rekammedik rm ON rm.Kasus_id = k.id\n" +
    "        \t\tJOIN penyakit p ON p.id = rm.Penyakit_id\n" +
    "        \t\tJOIN pasien pas ON pas.id = rm.Pasien_id\n" +
    "        \t\tJOIN rm_gj rmgj ON rmgj.RM_id = rm.id\n" +
    "        \t\tJOIN gejala g ON g.id = rmgj.Gj_id\n" +
    "        \tWHERE k.type = 'BASIS_KASUS' AND pas.woman = :woman\n" +
    "        \tGROUP BY k.id";

    protected final String NATIVE_SQL_FIND_KASUS = "" +
    "SELECT k.id as id, rm.id as rmid, p.id as pid, p.nama, k.type" +
    "     FROM kasus k" +
    "       JOIN rekammedik rm ON rm.Kasus_id = k.id" +
    "       JOIN penyakit p ON p.id = rm.Penyakit_id";

    protected final String NATIVE_SQL_FIND_ONE_KASUS = "" +
    "SELECT k.id as id, rm.id as rmid, p.id as pid, p.nama, k.type" +
    "     FROM kasus k" +
    "       JOIN rekammedik rm ON k.id = rm.id" +
    "       JOIN penyakit p ON p.id = rm.Penyakit_id" +
    "       WHERE k.id = :id";


    protected final String MYSQL_UPDATE_REKAM_MEDIK = "UPDATE rekammedik SET penyakit_id = :idPenyakit WHERE id = :id";
    protected final String MYSQL_GET_REKAM_MEDIK_ID_BY_KASUS = "SELECT rm.id as id FROM kasus k JOIN rekammedik rm ON k.rekamMedik_id = rm.id WHERE k.id = :id";
    
    public KasusRepo(@CurrentSession EntityManager em) {
        this.em = em;
    }

    @Transactional
    public Long create(KasusCreate payload) {
        RekamMedik rekamMedik = this.em.getReference(RekamMedik.class, payload.getIdRekamMedik());
        Kasus kasus = new Kasus();
        kasus.setRekamMedik(rekamMedik);
        kasus.setType(payload.getType());
        kasus.setSimilarity(payload.getSimilarity());
        this.em.persist(kasus);

        rekamMedik.setKasus(kasus);
        this.em.merge(rekamMedik);

        return kasus.getId();
    }

    @Transactional
    public void revise(Long idKasus, Long idPenyakit) {
        Kasus kasus = this.em.find(Kasus.class, idKasus);
        Query getRmQuery = em.createQuery(MYSQL_GET_REKAM_MEDIK_ID_BY_KASUS);
        getRmQuery.setParameter("id", idKasus);

        // Get Rekam Medik Id.
        Object[] rmIdResult = (Object[]) getRmQuery.getSingleResult();
        Long rmId = (Long) rmIdResult[0];

        // Update Penyakit in Rekam Medik.
        Query updateRekamMedikQuery = em.createQuery(MYSQL_UPDATE_REKAM_MEDIK);
        updateRekamMedikQuery.setParameter("idPenyakit", idPenyakit);
        updateRekamMedikQuery.setParameter("id", rmId);
        updateRekamMedikQuery.executeUpdate();

        // Update similarity
        kasus.setSimilarity(1D);
        em.merge(kasus);
    }

    @Transactional
    public void delete(Long id) {
        this.em.remove(this.em.getReference(Kasus.class, id));
    }

    @Transactional
    public List<Kasus> find() {
        return this.em.createQuery(JPQL_FIND_KASUS)
                      .getResultList();
    }

    @Transactional
    public Page<KasusDTO> find(Pageable pageable) {
        Query query = this.em.createNativeQuery(NATIVE_SQL_FIND_KASUS);
        List<KasusDTO> dtos = query.unwrap(org.hibernate.query.Query.class)
             .setResultTransformer(dtoTransformer)
             .getResultList();
        return Page.of(dtos, pageable, dtos.size());
    }

    @Transactional
    public Optional<Kasus> findOneWithRelations(Long id) {
        return this.em.createQuery(JPQL_FIND_KASUS_WITH_RELATIONS)
            .setParameter("id", id)
            .getResultStream()
            .findFirst();
    }

    @Transactional(readOnly = true)
    public Optional<KasusDTO> findOne(Long id) {
        Query query = this.em.createNativeQuery(NATIVE_SQL_FIND_ONE_KASUS);
        return query
            .setParameter("id", id)
            .unwrap(org.hibernate.query.Query.class)
            .setResultTransformer(dtoTransformer)
            .getResultStream()
            .findFirst();
    }

    @Transactional
    public List<Row> findForSexInBasisKasus(boolean woman) {
        int isWoman = woman ? 1 : 0;
        Query query = this.em.createNativeQuery(NATIVE_SQL_FIND_KASUS_FOR_SEX);
        List<Row> rows = query
            .setParameter("woman", isWoman)
            .unwrap(org.hibernate.query.Query.class)
            .setResultTransformer(rowTransformer)
            .getResultList();
        return rows;
    }

    private static KasusDTO convertKasus(Object[] tuple) {
        KasusDTO dto = new KasusDTO();
        dto.setId( ((BigInteger) tuple[0]).longValue() );
        dto.setRekamMedikId( ((BigInteger) tuple[1]).longValue() );
        dto.setPenyakitId( ((BigInteger) tuple[2]).longValue() );
        dto.setPenyakitNama( (String) tuple[3] );
        dto.setType( TipeKasus.BASIS_KASUS );
        return dto;
    }

    private static Row convertToRow(Object[] tuple) {
        Long kid = ((BigInteger) tuple[0]).longValue();
        Long pid = ((BigInteger) tuple[2]).longValue();
        String[] gidsString = ((String) tuple[5]).split(",");
        Long[] gids = Arrays.stream(gidsString)
             .map(Long::valueOf)
             .toArray(Long[]::new);
        return Row.Builder
            .id( kid )
            .attributes(gids)
            .target(pid);
    }

    private static final ResultTransformer dtoTransformer = new ResultTransformer() {
        @Override
        public Object transformTuple(Object[] tuple, String[] aliases) {
            return convertKasus(tuple);
        }

        @Override
        public List transformList(List collection) {
            return collection;
        }
    };

    private static final ResultTransformer rowTransformer = new ResultTransformer() {
        @Override
        public Object transformTuple(Object[] tuple, String[] aliases) {
            return convertToRow(tuple);
        }

        @Override
        public List transformList(List collection) {
            return collection;
        }
    };
}

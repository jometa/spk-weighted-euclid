package mnjava.derin.domain;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.*;
import io.micronaut.spring.tx.annotation.Transactional;
import io.micronaut.validation.Validated;

import java.util.Optional;

@Controller("/api/v1/penyakit")
@Validated
public class PenyakitController {

    protected PenyakitRepo penyakitRepo;

    public PenyakitController(PenyakitRepo penyakitRepo) {
        this.penyakitRepo = penyakitRepo;
    }

    @Post("/")
    public Long create(@Body Penyakit payload) {
        Penyakit penyakit = this.penyakitRepo.save(payload);
        return penyakit.getId();
    }

    @Put("/{id}")
    @Transactional
    public void update(Long id, @Body Penyakit payload) {
        Optional<Penyakit> optional = this.penyakitRepo.findById(id);
        optional.ifPresent(penyakit -> {
            penyakit.setNama(payload.getNama());
            penyakit.setDeskripsi(payload.getDeskripsi());
            penyakit.setSolusi(payload.getSolusi());
            this.penyakitRepo.save(penyakit);
        });
    }

    @Delete("/{id}")
    public void delete(Long id) {
        this.penyakitRepo.deleteById(id);
    }

    @Get("/")
    public Page<Penyakit> find(
        @QueryValue(value = "from", defaultValue = "0") Integer from,
        @QueryValue(value = "size", defaultValue = "20") Integer size
    ) {
        return this.penyakitRepo.findAll(Pageable.from(from, size));
    }

    @Get("/{id}")
    public Optional<Penyakit> findOne(Long id) {
        return this.penyakitRepo.findById(id);
    }
}

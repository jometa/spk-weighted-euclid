package mnjava.derin.domain;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository("domain")
public abstract class GejalaRepo implements PageableRepository<Gejala,Long> {

    private final EntityManager entityManager;

    public GejalaRepo(@CurrentSession EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NonNull
    public List<Gejala> findBySexAndKeyword(@NonNull String sex, @NonNull String keyword) {
        String queryS = "SELECT g FROM Gejala g WHERE lower(g.nama) LIKE lower(concat('%', :keyword, '%')) AND ";
        switch (sex) {
            case "woman":
                queryS = queryS + "g.woman = TRUE";
                break;
            case "man":
                queryS = queryS + "g.man = TRUE";
                break;
            default:
                throw new RuntimeException("Unknwon sex: " + sex);
        }
        Query query = this.entityManager.createQuery(queryS);
        query.setParameter("keyword", keyword);
        List<Gejala> results = query.getResultList();
        return results;
    }
}

package mnjava.derin.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Table(name="kasus")
@Entity
public class Kasus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name="similarity", nullable = true)
    protected Double similarity;

    @Column(name="type", nullable = false)
    @Enumerated(EnumType.STRING)
    protected TipeKasus type;

    @JsonIgnore
    @OneToOne(mappedBy = "kasus", fetch = FetchType.LAZY)
    protected RekamMedik rekamMedik;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Double similarity) {
        this.similarity = similarity;
    }

    public TipeKasus getType() {
        return type;
    }

    public void setType(TipeKasus type) {
        this.type = type;
    }


    public RekamMedik getRekamMedik() {
        return rekamMedik;
    }

    public void setRekamMedik(RekamMedik rekamMedik) {
        this.rekamMedik = rekamMedik;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kasus kasus = (Kasus) o;

        return getId() != null ? getId().equals(kasus.getId()) : kasus.getId() == null;
    }

    @Override
    public int hashCode() {
        return 0;
    }
}

package mnjava.derin;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import mnjava.derin.algo.C45;
import mnjava.derin.algo.Row;
import mnjava.derin.algo.WKnn;
import mnjava.derin.domain.Kasus;
import mnjava.derin.domain.KasusRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller("/api/v1/kfold")
public class KFoldController {

    protected KasusRepo kasusRepo;

    public KFoldController(KasusRepo kasusRepo) {
        this.kasusRepo = kasusRepo;
    }

    @Get("/{nfold}")
    public List<PartResult> kfold(@PathVariable("nfold") Integer nfold) {
        // Get all kasus
        final List<Kasus> all = this.kasusRepo.find();

        // Split data into nfold
        final List<List<Kasus>> parts = Util.split(all, nfold);

        List<PartResult> results = new ArrayList<>();

        for (int i = 0; i < nfold; i++) {
            List<Kasus> testing = parts.get(i);
            final int _i = i;

            List<List<Kasus>> trainparts = IntStream.range(0, nfold).filter(j -> j != _i)
                         .mapToObj(j -> parts.get(j))
                         .collect(Collectors.toList());

            // Convert to kasus
            List<Kasus> trainKasus = Util.join(trainparts);

            // Set up part result
            PartResult partResult = new PartResult();

            // Set up temp variables to hold hit
            int totalHit = 0;
            double totalSimilarity = 0D;

            for (int j = 0; j < testing.size(); j++) {

                Kasus kasus = testing.get(j);
                boolean is_current_woman = kasus.getRekamMedik().getPasien().getWoman();

                // Filter by sex
                List<Row> trainRows = trainKasus.stream()
                          .filter(k -> k.getRekamMedik().getPasien().getWoman() == is_current_woman)
                          .map(k -> Row.fromKasus(k))
                          .collect(Collectors.toList());

                Row testRow = Row.fromKasus(kasus);

                Map<Long, Double> weights = C45.run(trainRows);

                // Find closest instance with W-KNN.
                Util.Pair<Row, Double> closest = WKnn.findClosest(testRow, trainRows, weights);
                Row targetRow = closest._0;
                double similarity = closest._1;
                boolean is_hit = targetRow.target == testRow.target;

                if (is_hit) {
                    totalHit += 1;
                }
                totalSimilarity += similarity;

            }

            partResult.hit = totalHit;
            partResult.totalData = testing.size();
            partResult.miss = partResult.totalData - partResult.hit;
            partResult.accuracy = (partResult.hit * 1D) / partResult.totalData * 100.0;
            partResult.similarity = (totalSimilarity / partResult.totalData) * 100.0;

            results.add(partResult);
        }

        return results;
        // For each i fold
        //      1. Set i fold as testing.
        //      2. Set other fold as training
        //      3. Run classification against each data in i fold.
    }

}

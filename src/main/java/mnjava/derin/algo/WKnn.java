package mnjava.derin.algo;

import mnjava.derin.Util;

import java.util.*;

public class WKnn {

    public static double compare(Set<Long> xs, Set<Long> ys, Map<Long, Double> weights) {
        Set<Long> union = new HashSet<>();
        union.addAll(xs);
        union.addAll(ys);
        double total = union.stream()
            .map(e -> {
                boolean inBoth = (xs.contains(e) && ys.contains(e));
                double x = inBoth ? 0 : 1;
                return x * weights.getOrDefault(e, 0d);
            })
            .mapToDouble(Double::doubleValue)
            .sum();
        double d = Math.sqrt(total);
        return 1D / (1D + d);
    }

    public static Util.Pair<Row, Double> findClosest(Row xs, List<Row> YS, Map<Long, Double> weights) {
        return YS.stream()
          .map(ys -> {
              double sim = compare(xs.attributes, ys.attributes, weights);
              return Util.Pair.of(ys, sim);
          })
          .max(Comparator.comparingDouble(a -> a._1))
          .get();
    }
}

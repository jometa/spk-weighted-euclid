package mnjava.derin.algo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class C45 {

    public static Map<Long, Double> run(List<Row> rows) {
        final C45Summary summary = new C45Summary();

        summary.setTotalData(rows.size());
        for (Row row : rows) {
            long target = row.target;
            summary.incTarget(target);
            for (long attr: row.attributes) {
                summary.incTargetAttr(target, attr);
            }
        }

        Set<Long> targets = summary.targets();
        double entropy = summary.totalEntropy();

        List<AttrGain> gains = summary.attributes()
            .stream()
            .map(a -> {
               double gain_0 = 0D;
               double gain_1 = 0D;
               for (long t : targets) {
                    gain_0 += summary.negEntropyTargetAttr(t, a);
                    gain_1 += summary.posEntropyTargetAttr(t, a);
               }
               double coef_0 = summary.negCountAttr(a) / summary.totalData;
               double coef_1 = summary.countAttr(a) / summary.totalData;
               double gain = entropy + (coef_0 * gain_0) + (coef_1 * gain_1);
               return new AttrGain(a, gain);
            })
            .collect(Collectors.toList());

        double totalGains = gains.stream().mapToDouble(AttrGain::getGain).sum();

        Map<Long, Double> result = new HashMap<>();
        gains.forEach(attrGain -> {
            double normGain = attrGain.gain / totalGains;
            result.put(attrGain.attr, normGain);
        });
        return result;
    }

    public static void test() {
        List<Row> rows = Stream.of(
            Row.Builder.id(1L).attributes(2, 4).target(1),
            Row.Builder.id(2L).attributes(1, 2, 3).target(2),
            Row.Builder.id(3L).attributes(1, 4, 5).target(3),
            Row.Builder.id(4L).attributes(1, 3).target(2),
            Row.Builder.id(5L).attributes(2, 3).target(2),
            Row.Builder.id(6L).attributes(1, 2).target(2),
            Row.Builder.id(7L).attributes(4, 5).target(3),
            Row.Builder.id(8L).attributes(2).target(2),
            Row.Builder.id(9L).attributes(1, 3, 5).target(3),
            Row.Builder.id(10L).attributes(1).target(2)
        ).collect(Collectors.toList());
        Map<Long, Double> result = run(rows);
    }

    public static class AttrGain {
        public long attr;
        public double gain;

        public AttrGain(long attr, double gain) {
            this.attr = attr;
            this.gain = gain;
        }

        public long getAttr() {
            return attr;
        }

        public void setAttr(long attr) {
            this.attr = attr;
        }

        public double getGain() {
            return gain;
        }

        public void setGain(double gain) {
            this.gain = gain;
        }

        @Override
        public String toString() {
            return "AttrGain{" +
            "attr=" + attr +
            ", gain=" + gain +
            '}';
        }
    }
}

package mnjava.derin.algo;

import mnjava.derin.domain.Kasus;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Row {

    public Long id;
    public Set<Long> attributes;
    public Long target;

    public Row(Long id, Set<Long> attributes, Long target) {
        this.id = id;
        this.attributes = attributes;
        this.target = target;
    }

    public Row() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "C45Row{" +
        "id=" + id +
        ", attributes=" + attributes +
        ", target=" + target +
        '}';
    }

    public static Row fromKasus(Kasus k) {
        Row row = new Row();
        row.id = k.getId();
        row.attributes = k.getRekamMedik().getGejalas().stream().map(g -> g.getId()).collect(Collectors.toSet());
        row.target = k.getRekamMedik().getPenyakit().getId();
        return row;
    }

    public static class Builder {
        public long id;
        public Set<Long> attributes;
        public Long target;

        public static Builder id(long id) {
            Builder builder = new Builder();
            builder.id = id;
            return builder;
        }


        public Builder attributes(Long ...attrs) {
            this.attributes = Arrays.stream(attrs).collect(Collectors.toSet());
            return this;
        }

        public Builder attributes(Integer ...attrs) {
            Long longs[] = Arrays.stream(attrs)
                .mapToLong(i -> 1l * i)
                .boxed()
                .toArray(Long[]::new);
            return this.attributes(longs);
        }

        public Row target(Long target) {
            this.target = target;
            return new Row(this.id, this.attributes, this.target);
        }


        public Row target(Integer target) {
            return this.target(target.longValue());
        }
    }
}

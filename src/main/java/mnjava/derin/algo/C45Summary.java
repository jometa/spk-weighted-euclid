package mnjava.derin.algo;

import mnjava.derin.Util;

import java.util.*;
import java.util.stream.Collectors;

public class C45Summary {

    protected Map<Key, Long> summaryTargetAttr = new HashMap<>();
    protected Map<Long, Long> summaryTarget = new HashMap<>();
    protected long totalData = 0L;

    public void setTotalData(long totalData) {
        this.totalData = totalData;
    }

    public void incTargetAttr(long target, long attr) {
        Key key = _key(target, attr);
        long currentCount = summaryTargetAttr.getOrDefault(key, 0L);
        summaryTargetAttr.put(key, currentCount + 1);
    }

    public void incTarget(long target) {
        long currentCount = summaryTarget.getOrDefault(target, 0l);
        summaryTarget.put(target, currentCount + 1);
    }

    public double countTargetAttr(long target, long attr) {
        return summaryTargetAttr.getOrDefault(_key(target, attr),0L);
    }

    public double negCountTargetAttr(long target, long attr) {
        return this.countTarget(target) - this.countTargetAttr(target, attr);
    }

    public double countTarget(long target) {
        return this.summaryTarget.getOrDefault(target, 0L);
    }

    public double countAttr(long attr) {
        return this.summaryTargetAttr
            .entrySet()
            .stream()
            .filter(entry -> attr == entry.getKey().attr)
            .mapToDouble(entry -> entry.getValue())
            .sum();
    }

    public double negCountAttr(long attr) {
        return totalData - this.countAttr(attr);
    }

    public double entropyTarget(long target) {
        double x = this.countTarget(target) / this.totalData;
        return -x * Util.log2(x) * x;
    }

    public double posEntropyTargetAttr(long target, long attr) {
        double x = this.countTargetAttr(target, attr) / this.totalData;
        if (Util.compare(x, 0)) return 0D;
        return -x * Util.log2(x) * x;
    }

    public double negEntropyTargetAttr(long target, long attr) {
        double x = this.negCountTargetAttr(target, attr) / this.totalData;
        if (Util.compare(x, 0)) return 0D;
        return -x * Util.log2(x) * x;
    }

    public double totalEntropy() {
        return this.targets()
            .stream()
            .map(t -> {
                double entropy = this.entropyTarget(t);
                return entropy;
            })
            .reduce((a, b) -> a + b)
            .orElse(0D);
    }

    public Set<Long> targets() {
        return this.summaryTargetAttr
            .keySet()
            .stream()
            .map(key -> key.target)
            .collect(Collectors.toSet());
    }

    public Set<Long> attributes() {
        return this.summaryTargetAttr
            .keySet()
            .stream()
            .map(key -> key.attr)
            .collect(Collectors.toSet());
    }

    static class Key {
        public final long target;
        public final long attr;
        private static final double delta = 0.001D;

        public Key(long target, long attr) {
            this.target = target;
            this.attr = attr;
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof Key)) {
                return false;
            }

            Key _other = (Key) other;
            if (!(_other.target == this.target)) return false;
            if (!(_other.attr == this.attr)) return false;
            return true;
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }

    public static Key _key(long target, long attr) {
        return new Key(target, attr);
    }

}

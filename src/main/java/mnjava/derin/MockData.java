package mnjava.derin;

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.spring.tx.annotation.Transactional;
import mnjava.derin.domain.*;
import mnjava.derin.domain.dto.KasusCreate;
import mnjava.derin.domain.dto.RekamMedikCreateOld;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller("/api/mock")
public class MockData {

    @PersistenceContext
    protected EntityManager em;
    protected RekamMedikRepo rekamMedikRepo;

    public MockData(
        @CurrentSession EntityManager em,
        RekamMedikRepo rekamMedikRepo
    ) {
        this.em = em;
        this.rekamMedikRepo = rekamMedikRepo;
    }

    @Transactional
    @Get
    public void insert() {
        List<Gejala> gejalalist = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Gejala g = new Gejala();
            String nama = "G" + i;
            g.setId((long) i);
            g.setNama(nama);
            g.setDeskripsi(nama);
            g.setWoman(true);
            g.setMan(true);
            this.em.merge(g);
            gejalalist.add(g);
        }

        List<Penyakit> penyakitList = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            Penyakit p = new Penyakit();
            String nama = "P" + i;
            p.setNama(nama);
            p.setDeskripsi(nama);
            p.setSolusi(nama);
            p.setWoman(true);
            p.setMan(true);
            this.em.persist(p);
            penyakitList.add(p);
        }

        // Flush here.
        this.em.flush();

        Penyakit penyakit = penyakitList.get(0);

        Pasien pasien = new Pasien();
        pasien.setNama("Derin Noraeni");
        pasien.setAlamat("Oeba");
        pasien.setWoman(true);
        pasien.setTempatLahir("Kupang");
        pasien.setTanggalLahir(LocalDate.of(1995, 10, 10));

        // Make rekam medik
        RekamMedik rekamMedik = new RekamMedik();
        rekamMedik.setTanggal(LocalDate.now());

        // Make kasus
        Kasus kasus = new Kasus();
        kasus.setSimilarity(1.0);
        kasus.setType(TipeKasus.BASIS_KASUS);

        // Join relations for penyakit
        penyakit.addRekamMedik(rekamMedik);

        // Join relations for pasien
        pasien.addRekamMedik(rekamMedik);

        // Join relations for Rekam Medik
        rekamMedik.setPasien(pasien);
        rekamMedik.setPenyakit(penyakit);
        for (Gejala g : gejalalist.subList(0, 5)) {
            g.addRekamMedik(rekamMedik);
            rekamMedik.addGejala(g);
        }

        // Join relations for Kasus
        kasus.setRekamMedik(rekamMedik);

        this.em.persist(pasien);
        this.em.persist(rekamMedik);
        this.em.persist(kasus);
    }
}

package mnjava.derin;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import mnjava.derin.algo.C45;
import mnjava.derin.algo.Row;
import mnjava.derin.algo.WKnn;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller("/api/algo")
public class C45Controller {

    protected FooC45 fooC45;

    public C45Controller(FooC45 fooC45) {
        this.fooC45 = fooC45;
    }

    @Get("/test")
    public void test() {
        List<Row> rows = Stream.of(
            Row.Builder.id(1L).attributes(2, 4).target(1),
            Row.Builder.id(2L).attributes(1, 2, 3).target(2),
            Row.Builder.id(3L).attributes(1, 4, 5).target(3),
            Row.Builder.id(4L).attributes(1, 3).target(2),
            Row.Builder.id(5L).attributes(2, 3).target(2),
            Row.Builder.id(6L).attributes(1, 2).target(2),
            Row.Builder.id(7L).attributes(4, 5).target(3),
            Row.Builder.id(8L).attributes(2).target(2),
            Row.Builder.id(9L).attributes(1, 3, 5).target(3),
            Row.Builder.id(10L).attributes(1).target(2)
        ).collect(Collectors.toList());
        Map<Long, Double> weights = C45.run(rows);

        Row xs = Row.Builder.id(0L).attributes(2, 5).target(-1L);
        Util.Pair<Row, Double> closest = WKnn.findClosest(xs, rows, weights);
        System.out.println("closest = " + closest);
    }

    @Get("/single")
    public void single() {

    }
}

package mnjava.derin;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import mnjava.derin.algo.C45;
import mnjava.derin.algo.Row;
import mnjava.derin.algo.WKnn;
import mnjava.derin.domain.Kasus;
import mnjava.derin.domain.KasusRepo;
import mnjava.derin.domain.RekamMedik;
import mnjava.derin.dto.ClassificationInput;
import java.util.List;
import java.util.Map;

@Controller("/api/v1/classification")
public class ClassificationController {

    protected KasusRepo kasusRepo;

    public ClassificationController(KasusRepo kasusRepo) {
        this.kasusRepo = kasusRepo;
    }

    @Get("/")
    public String foo() {
        return "FOObar";
    }

    @Post("/")
    public ClassificationResult single(ClassificationInput payload) {
        // 1. Get all data in basis kasus for current payload.
        List<Row> rows = this.kasusRepo.findForSexInBasisKasus(payload.isWoman());

        // 2. Calculate weights with C45 Indexing
        Map<Long, Double> weights = C45.run(rows);

        Row xs = Row.Builder.id(0L)
            .attributes(
                payload.getIdListGejala()
                    .stream()
                    .toArray(Long[]::new)
            ).target(-1L);

        // 3. Find closest instance with W-KNN.
        Util.Pair<Row, Double> closest = WKnn.findClosest(xs, rows, weights);

        Kasus kasus = this.kasusRepo.findOneWithRelations(closest._0.getId()).get();
        RekamMedik rm = kasus.getRekamMedik();

        ClassificationResult result = new ClassificationResult();
        result.kasus = kasus;
        result.rekamMedik = rm;
        result.similarity = closest._1;

        return result;
    }

}

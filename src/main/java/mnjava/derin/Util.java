package mnjava.derin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Util {

    public static final double DELTA = 0.0001;

    public static boolean compare(double a, double b, double delta) {
        return Math.abs(a - b) < delta;
    }

    public static boolean compare(double a, double b) {
        return compare(a, b, DELTA);
    }

    public static double log2(double x) {
        if (compare(x, 0D)) return 0d;
        return Math.log(x) / Math.log(2);
    }

    public static <T> List<List<T>> split(List<T> list, int n) {
        List<List<T>> parts = IntStream.range(0, n)
                                              .mapToObj(i -> new ArrayList<T>())
                                              .collect(Collectors.toList());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            T t = list.get(i);
            int j = i % n;
            parts.get(j).add(t);
        }
        return parts;
    }

    public static <T> List<T> join(List<List<T>> list) {
        List<T> result = new ArrayList<>();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            list.get(i).forEach(item -> {
                result.add(item);
            });
        }
        return result;
    }

    public static class Pair<S, T> {
        public S _0;
        public T _1;

        public static <S, T> Pair<S, T> of(S s, T t) {
            Pair<S, T> pair = new Pair    <>();
            pair._0 = s;
            pair._1 = t;
            return pair;
        }

        @Override
        public String toString() {
            return "Pair{" +
            "_0=" + _0 +
            ", _1=" + _1 +
            '}';
        }
    }

}

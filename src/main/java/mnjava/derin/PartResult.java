package mnjava.derin;

public class PartResult {
    public double similarity = 0D;
    public int totalData = 0;
    public int hit = 0;
    public int miss = 0;
    public double accuracy = 0D;
}

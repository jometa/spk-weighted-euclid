package mnjava.derin.dto;

import java.time.LocalDate;
import java.util.List;

public class ClassificationInput {
    protected List<Long> idListGejala;
    protected boolean woman;
    protected String nama;
    protected String tempatLahir;
    protected String alamat;
    protected LocalDate tanggalLahir;

    public List<Long> getIdListGejala() {
        return idListGejala;
    }

    public void setIdListGejala(List<Long> idListGejala) {
        this.idListGejala = idListGejala;
    }

    public boolean isWoman() {
        return woman;
    }

    public void setWoman(boolean woman) {
        this.woman = woman;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public LocalDate getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(LocalDate tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
}

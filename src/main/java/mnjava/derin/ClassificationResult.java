package mnjava.derin;

import mnjava.derin.domain.Kasus;
import mnjava.derin.domain.RekamMedik;

public class ClassificationResult {
    public Kasus kasus;
    public RekamMedik rekamMedik;
    public Double similarity;
}
